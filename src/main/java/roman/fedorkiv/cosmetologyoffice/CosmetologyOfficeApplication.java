package roman.fedorkiv.cosmetologyoffice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import roman.fedorkiv.cosmetologyoffice.entity.Service;
import roman.fedorkiv.cosmetologyoffice.repository.ServiceRepository;
import roman.fedorkiv.cosmetologyoffice.repository.UserRepository;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class CosmetologyOfficeApplication {

        @Autowired
        public ServiceRepository serviceRepository;

        @Autowired
        public UserRepository userRepository;

        @PostConstruct
        public  void init(){
            Service service = serviceRepository.findById(1L).orElse(null);


            userRepository.findAll().forEach(System.out::println);

        }
    public static void main(String[] args) {
        SpringApplication.run(CosmetologyOfficeApplication.class, args);
    }

}
