package roman.fedorkiv.cosmetologyoffice.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    private String name;

    private Date date;


    @ManyToMany
    private List<Cosmetolog> cosmetologists = new ArrayList<>();

    @OneToMany(mappedBy = "service")
    private List<Booking> bookings = new ArrayList<>();

}
