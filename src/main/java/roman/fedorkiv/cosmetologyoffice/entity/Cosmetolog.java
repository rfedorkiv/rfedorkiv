package roman.fedorkiv.cosmetologyoffice.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor


@Entity
public class Cosmetolog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(unique = true)
    private String specialization;

    @ManyToMany(mappedBy = "cosmetologists")
    private List<Service> service = new ArrayList<>();

    @OneToMany(mappedBy = "cosmetolog")
    private List<Booking> bookings = new ArrayList<>();

}
