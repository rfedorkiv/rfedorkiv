package roman.fedorkiv.cosmetologyoffice.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String institution;

    private String department;



    @ManyToOne
    private User user;

    @ManyToOne
    private Cosmetolog cosmetolog;

    @ManyToOne
    private Service service;

}
