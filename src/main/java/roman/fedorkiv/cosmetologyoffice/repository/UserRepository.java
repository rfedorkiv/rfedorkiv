package roman.fedorkiv.cosmetologyoffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import roman.fedorkiv.cosmetologyoffice.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByName(String name);


//    List<User> findByEmailAddressAndLastname(String emailAddress, String name);


}
