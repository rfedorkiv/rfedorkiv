package roman.fedorkiv.cosmetologyoffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import roman.fedorkiv.cosmetologyoffice.entity.Service;

public interface ServiceRepository extends JpaRepository<Service, Long> {
    Service findByName(String name);
}