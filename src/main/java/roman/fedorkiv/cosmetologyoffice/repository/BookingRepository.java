package roman.fedorkiv.cosmetologyoffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import roman.fedorkiv.cosmetologyoffice.entity.Booking;

public interface BookingRepository  extends JpaRepository<Booking, Long> {

}
