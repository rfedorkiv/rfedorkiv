package roman.fedorkiv.cosmetologyoffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import roman.fedorkiv.cosmetologyoffice.entity.Cosmetolog;

public interface CosmetologRepository extends JpaRepository<Cosmetolog, Long> {
    Cosmetolog findByName(String name);
}
